## Getting Started

To start, fork this repository to your own gitlab repo and configure
things:

 1. change in .gitlab-ci.yml the following variables as needed:
    - S3_INDEXURL: s3://pantace-asac/pub/pvr2img-example/index.json
    - S3_RELEASECHANNEL: stable
    - PVR_REFERENCE: pantahub-ci/rpi64_5_10_y_initial_latest
 2. Configure your secrets as "protected" and "masked" variables in
    gitlab Settings -> CI/CD -> Variables
    - AWS_SECRET_ACCESS_KEY
    - AWS_ACCESS_KEY_ID
 3. Setup "Protected Tags" in Settings -> Repository. For instance
    use the "wildcard" '*' to ensure that all tags can build images
    tha can be uploaded to s3
 4. If the manifest repository or any git trees are in private repos
    you can configure a gitlab user using the two variables:
    - PH_CIBOT_GITLAB_USER
    - PH_CIBOT_GITLAB_TOKEN
    Again, remember to Protect these and Mask them.
 
Remember to setup your S3 bucket like described in the next
section (Setup S3).

Once you have your own repo you can produce images by

 1. tagging your repository as you wish. your tag name will be used
    as your releasename aka version
 2. run a pipeline manual. This offers you the max flexibility. You
    can choose custom pvr repo references, custom s3 credentials,
    custon releasechannel names etc. all through a gitlab CI pipeline
    run form.

### Tagging release

Once tagged, the CI will build your image using the PVR_REFERENCE
url as input and will publish it to your S3_INDEXURL index.

Note that S3_RELEASECHANNEL can be omitted if you only use channels
produced by pantafleet as that one adds the preferred channel
name alongside the pvr repository step/release.

If you want to have multiple release channels we recommend
to create multiple branhces of this repo and change the PVR_REFERENCE
and S3_RELEASECHANNEL in each branch as needed.

Tagging on a branch will then produice an image based on that
info and publish it into the channel.

### Running Manual Pipeline

Simple use the CI/CD -> Pipeline -> Run pipeline button in your repo.

There you will find a prefilled form that allows you to customize
the pipeline in all kinds and fashions. Amgonst other things
you can use this to reproduce any build and tag if you ever need so.

It is noteworthy that the PERSONAL_AWS_ fields presented are optional
if you have set the global AWS_ fields in the CI/CD UI of gitlab.

## Setup S3

To use pvr2img and publish the resulting artifacts into s3 hosted
releaseindexes you will have to setup your s3 account in a certain
manner.

In particular its important to create a user that has write
access to the bucket you want to use and if you want to use the
resulting published releaseindex in hub.pantacor.com download-wizard
you will also have to make access to these artifacts available
for anonymous users.

To make this easier hier two simply s3 access policies that
we have put in place in our test account:

### Example S3 Bucket Policy

First when creating a bucket, remember to disable the "Block ALL public"
feature. This will prevent creating policies that would allow
public download.

In our example we created the bucket "pantace-asac".

Here you would use the following policy to allow public
read of anything published beneath the `/pub/` folder.:

```
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "PublicRead",
            "Effect": "Allow",
            "Principal": "*",
            "Action": [
                "s3:GetObject",
                "s3:GetObjectVersion"
            ],
            "Resource": "arn:aws:s3:::pantace-asac/pub/*"
        }
    ]
}
```

### Example S3 Bucket CORS Policy

To allow hub and other websites to access the bucket content
you will also have to allow this through CORS in the Bucket
Permissions tab under section "Cross-origin resource sharing (CORS)":


```
[
    {
        "AllowedHeaders": [
            "*"
        ],
        "AllowedMethods": [
            "GET",
            "HEAD"
        ],
        "AllowedOrigins": [
            "*"
        ],
        "ExposeHeaders": []
    }
]
```


### Example IAM User policy for upload bot

for uploading we recommend you create a user or group in IAM that
you use exclusively for this task.

To give that user the right permissions to upload into your bucket
you need again a policy attached to the user.

For our example we created a user called "pantace-asac-bot1" which we
then attached the following custom policy to:

```
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": "s3:ListAllMyBuckets",
            "Resource": "arn:aws:s3:::pantace-asac"
        },
        {
            "Effect": "Allow",
            "Action": [
                "s3:ListBucket",
                "s3:GetBucketLocation"
            ],
            "Resource": "arn:aws:s3:::pantace-asac"
        },
        {
            "Effect": "Allow",
            "Action": [
                "s3:PutObject",
                "s3:PutObjectAcl",
                "s3:GetObject",
                "s3:GetObjectAcl",
                "s3:DeleteObject"
            ],
            "Resource": "arn:aws:s3:::pantace-asac/*"
        }
    ]
}
```

Remember to change the "Resource" field so it points to your
s3 bucket name.

The AWS_ secrets configured in "Getting Started" are the ones
from such bot user account.


## Run pvr2img for local builds

You can run pvr2img yourself. Use the following arguments to do so:

```
AWS_SECRET_ACCESS_KEY=XXXXX \
AWS_ACCESS_KEY_ID=YYYYY \
	pvr2img pantahub-ci/rpi64_5_10_y_initial_latest \
		s3://YOURBUCKETNAME/pub/index.json \
		RELEASENAME \
		RELEASECHANNELNAME
```

This will produce a working directory in /tmp/pvr2img.* which contains
all the release artifacts. It will also upload the bits into
the s3 bucket given the path and enlist the release in the provided
release index.json


#!/bin/bash

set -ex

cmd=`basename $0`

AWS=${AWS:=aws}

usage() {

	[ -z "$1" ] || echo "$1"
	echo ""
	echo "Usage: $cmd <pvrrepo> [s3://<bucket>[/<path]/<channelindex>.json <release> <releasechannel>]"
	echo ""
	echo "To authenticate against s3, provide ACCESS_KEYID etc. as env"
	echo "    - AWS_ACCESS_KEY_ID"
	echo "    - AWS_SECRET_ACCESS_KEY"
	echo "    - AWS_DEFAULT_REGION"
	echo

	exit 128
}

echo_err() {
    echo $@ 1>&2
}

clone_device() {
	_pvrrepo=$1
	_devicedir=$2

	if [ -z "$_pvrrepo" ]; then
		usage "ERROR: must specify pvrrepo for clone_device"
	fi
	if [ -z "$_devicedir" ]; then
		usage "ERROR: must specify device directory for clone_device"
	fi

	rm -rf $_devicedir
	PVR_OBJECTS_DIR=${PVR_OBJECTS_DIR:-$pvrobjdir} pvr clone $_pvrrepo $_devicedir
}

read_device_meta() {
	_devicedir=$1

	if ! [ -f $_devicedir/bsp/build.json ]; then
		usage "ERROR: device must have a bsp/build.json file"
	fi

	manifest_project=`cat $_devicedir/bsp/build.json | jq -r .project`
	manifest_platform=`cat $_devicedir/bsp/build.json | jq -r .platform`
	manifest_target=`cat $_devicedir/bsp/build.json | jq -r .target`
	manifest_commit=`cat $_devicedir/bsp/build.json | jq -r .commit`
	manifest_branch=`cat $_devicedir/bsp/build.json | jq -r .branch`
	manifest_altrepogroups=`cat $_devicedir/bsp/build.json | jq -r .altrepogroups`
}

clone_device_manifest() {
	_repodir=$1
	_devicedir=$2

	mkdir -p $_repodir
	pushd $_repodir
	set -x
	repo init -v -u https://gitlab.com/$manifest_project \
	     ${REPO_MIRROR:+--reference=$REPO_MIRROR} \
	     ${REPO_EXTRA_FLAGS} \
	     --no-clone-bundle \
	     -m release.xml \
	     --groups "runtime,${manifest_altrepogroups:-$manifest_platform}"

	if [ -n "$manifest_branch" ]; then
		sh -c "cd .repo/manifests && git fetch origin $manifest_branch && git checkout $manifest_branch"
	fi

	if [ -n "$manifest_commit" ]; then
		sh -c "cd .repo/manifests && git reset --hard $manifest_commit"
	fi

	popd
}

repo_sync() {
	_repodir=$1
	pushd $_repodir
	repo sync -v -j ${REPO_J:-10} --nmu
	popd
}

build_target() {
    _target=$1
    _repodir=$2
    _devicedir=$3

    pushd $_repodir

    repo status | cat
    repo manifest -r | cat

    eval "$PVR_BUILD_OPTIONS ./build.docker.sh $_target"
    eval "$PVR_BUILD_OPTIONS ./build.docker.sh $_target dump-xml"
}

make_release_json() {
    _pipelinedir=$1
    _s3jsonuri=$2

    _pipelineid=`basename $_pipelinedir`

    _JSONTMPL_RELEASE='{
	server: $server,
	pvrdevice: $pvrdevice,
	pvrrepo: $pvrrepo,
	pvrurl: $pvrurl,
	pipeline: $pipeline,
	devices: []
    }'

    _JSONTMPL_DEVICE='{
	job: $job,
	platform: $platform,
	target: $target,
	devicenick: $devicenick,
	devicerev: $devicerev,
	images: []
    }'

    _JSONTMPL_IMAGE='{
	name: $name,
	options: $options,
	url: $url,
	checksum: $checksum,
	tezi_url: $tezi_url,
	tezi_checksum: $tezi_checksum,
	rootfs_url: $rootfs_url,
	rootfs_checksum: $rootfs_checksum
    }'

    pvrurl=`pvr remoteinfo $pvrrepo | jq -r '.["json-get-url"]'`
    pvrserver=`echo $pvrurl | sed -e 's#https://\([[:alnum:]:.]*\)/.*#\1#'`
    pvrdevice=`echo $pvrurl | sed -e 's#https://\([[:alnum:]:.]*\)/trails/\(.*\)/steps.*#\2#'`
    pvrdevicerev=`echo $pvrurl | sed -e 's#https://.*/steps/##;s#/state.*##'`
    pvrdevicenick=`echo $pvrrepo | sed -e 's#https://\([[:alnum:]:.]*\)/##'`
    pvrdevicenick=`echo $pvrdevicenick | sed -e 's#^trails/##'`

    jq -n \
	--arg server "$pvrserver" \
	--arg pipeline "$_pipelineid" \
	--arg pvrdevice "$pvrdevice" \
	--arg pvrrepo "${pvrrepo%/steps/*}" \
	--arg pvrurl "$pvrurl" \
	"$_JSONTMPL_RELEASE" > $_pipelinedir/release.json

    jq -n \
	--arg job "$cijob" \
	--arg platform "$manifest_platform" \
	--arg target "$manifest_target" \
	--arg devicenick "$pvrdevicenick" \
	--arg devicerev "$pvrdevicerev" \
	"$_JSONTMPL_DEVICE" > $_pipelinedir/device.json

    shafile=`ls $_pipelinedir/*.sha256sum | head -n1`
    uploadfile=`cat $shafile | head -n 1 | awk '{ print $2 }'`
    uploadsha=`cat $shafile | head -n 1 | awk '{ print $1 }'`

    jq -n \
	--arg name "default" \
	--arg options "$PVR_BUILD_OPTIONS" \
	--arg url "${s3urlprefix}${uploadfile}" \
	--arg checksum "$uploadsha" \
	--arg tezi_url "$tezi_url" \
	--arg tezi_checksum "$tezi_checksum" \
	--arg rootfs_url "$rootfs_url" \
	--arg rootfs_checksum "$rootfs_checksum" \
	"$_JSONTMPL_IMAGE" > $_pipelinedir/image.json

    # append to images and devices fields to make a full release doc
    cat $_pipelinedir/device.json | jq --argjson img "$(<${_pipelinedir}/image.json)" ' .images += [ $img ]' > $_pipelinedir/device_image.json
    cat $_pipelinedir/release.json | jq --argjson img "$(<${_pipelinedir}/device_image.json)" ' .devices += [ $img ]' > $_pipelinedir/release_merged.json
}

_assemble_pipeline_img() {
    _pipelinedir=$1
    _repodir=$2
    _platform=$3

    _imgname=$_target-pv-$releasechannel-$release.img
    _pipeimg=$_pipelinedir/$_imgname

    cat $_repodir/out/$_target/$_target-pv-*.img > $_pipeimg
    pvr2imgsplit $_pipeimg
    cat $_pipeimg | xz > $_pipeimg.xz
    rm -f $_pipelinedir/*.sha256sum
    _shatmp=`mktemp -t sha256sum.XXXXXXXXX`
    sh -c "cd $_pipelinedir; sha256sum ${_imgname}.xz *" > $_shatmp
    mv $_shatmp ${_pipelinedir}/${_target}-pv-${releasechannel}-${release}.sha256sum
}

_assemble_pipeline_rootfs() {
    _pipelinedir=$1
    _repodir=$2
    _target=$3

    echo_err "NOT IMPLEMENTED: _assemble_pipeline_rootfs"
    exit 127
}

_assemble_pipeline_envpkg() {
    _pipelinedir=$1
    _repodir=$2
    _target=$3

    echo_err "NOT IMPLEMENTED: _assemble_pipeline_envpkg"
    exit 128
}

_assemble_pipeline_tezi() {
    _pipelinedir=$1
    _repodir=$2
    _target=$3

    echo_err "NOT IMPLEMENTED: _assemble_pipeline_tezi"
    exit 129
}

assemble_pipeline() {
    _pipelinedir=$1
    _repodir=$2
    _target=$3
    _imgtype=$4

    if [ -z "$_imgtype" ]; then
	# configdir
	_configdir=`xpath -e '/alchemy/target/var[@name="CONFIG_DIR"]/value/text()' $_repodir/out/$_target/alchemy-database.xml`
	# gather type from build.env
	_imgtype=`sh -c ". $_configdir/build.env 1>&2; echo \\$PVBUILD_IMAGE_TYPE"`
    fi

    case $_imgtype in
	block)
	    _assemble_pipeline_img $_pipelinedir $_repodir $_target
	    ;;
	tezi)
	    _assemble_pipeline_tezi $_pipelinedir $_repodir $_target
	    ;;
	rootfs)
	    _assemble_pipeline_rootfs $_pipelinedir $_repodir $_target
	    ;;
	envpkg)
	    _assemble_pipeline_envpkg $_pipelinedir $_repodir $_target
	    ;;
	*)
	    echo_err "Invalid pipeline type $_type"
	    exit 130
	    ;;
    esac
}

publish_pipeline() {
    _pipelinedir=$1

    # lets ensure we can bootstrap new stream files as empty docs ...
    if ! $AWS s3 cp $s3jsonuri $_pipelinedir/../$releaseindex.orig; then
	echo "{}" > $_pipelinedir/../$releaseindex.orig
    fi

    # merge new release into release stream file ...
    cat $_pipelinedir/../$releaseindex.orig | \
	jq --argjson releasemerged \
	   "$(<${_pipelinedir}/release_merged.json)" '.["'${releasechannel}'"] += [$releasemerged]' \
	   > $_pipelinedir/../$releaseindex

    # lets copy bits up
    $AWS s3 cp --recursive --exclude '*.json' --exclude '*.img' $_pipelinedir $s3cpurl

    # last copy the release stream manifest
    $AWS s3 cp $_pipelinedir/../$releaseindex $s3jsonuri.new
    $AWS s3 mv $s3jsonuri.new $s3jsonuri
}

parse_s3jsonuri() {
    _s3jsonuri=$1
    script=`echo $_s3jsonuri | sed -e 's#s3://\([^/]*\)/\(.*\)#aws_bucket=\1\naws_path=\2\n#'`
    eval "$script"
    aws_dir=`dirname $aws_path`
    aws_dir=${aws_dir%%/}
    releaseindex=`basename $aws_path`
    echo AWSBUCKET: $aws_bucket
    echo AWSPATH: $aws_path
    echo AWSDIR: $aws_dir
    echo RELEASEINDEX: $releaseindex
    export aws_dir releaseindex aws_path aws_bucket
}

pvrrepo=$1
s3jsonuri=$2
release=${3:-latest}
releasechannel=${4}
cijob=${CI_JOB_ID:-localbuild}

if [ -z "$pvrrepo" ]; then
	usage "ERROR: provide a pvrrepo as first argument."
fi
if [ -n "$s3jsonuri" ] && [ -z "$AWS_ACCESS_KEY_ID" ]; then
	usage 'ERROR: AWS_ACCESS_KEY_ID and <bucket> must be set to enable s3 uploads; see --help'
fi
if [ -n "$s3jsonuri" ] && [ -z "$AWS_SECRET_ACCESS_KEY" ]; then
	usage "ERROR: AWS_SECRET_ACCESS_KEY and target bucket must be set to enable s3 uploads; see --help"
fi

if [ -z "$releasechannel" ]; then
	stepinfo=`pvr stepinfo $pvrrepo`
	wantedchannel=`echo $stepinfo | jq '.meta.pvr2img.channelname'`
	if [ -z "$wantedchannel" ] || [ "$wantedchannel" = "null" ]; then
	    releasechannel=${4:-localrelease}
	else
	    releasechannel=$wantedchannel
	fi
fi

# AWS_DEFAULT_REGION=us-west-2
imgdir=${IMGDIR:-`mktemp -d -t pvr2img.XXXXXXXXX`}
echo "Image Work Dir in $imgdir"
pushd $imgdir

repodir=$imgdir/repo
devicedir=$imgdir/device
pvrobjdir=$imgdir/pvr-objects
pipelineid=${CI_PIPELINE_ID:-`date +%s`}
if [ -z "$NEWPIPELINE" ] && [ -n "$IMGDIR" ] && [ -e $imgdir/last-pipeline-id.txt ]; then
    pipelineid=$(cat $imgdir/last-pipeline-id.txt)
fi
echo $pipelineid > $imgdir/last-pipeline-id.txt
pipelinedir=${PIPELINEDIR:-$imgdir/pipeline/$pipelineid}
mkdir -p $pipelinedir

if [ -n "$s3jsonuri" ]; then
    parse_s3jsonuri $s3jsonuri
else
    releaseindex="pvr2img.index.json"
fi

# extract s3 elements from s3jsonuri
s3cpurl=
s3urlprefix=
if [ -n "$s3jsonuri" ]; then
    _relpath="releases/${release}/`basename $pipelinedir`"
    s3cpurl="s3://${aws_bucket}/${aws_dir}/${_relpath}"
    s3urlprefix="https://${aws_bucket}.s3.amazonaws.com/${aws_dir}/${_relpath}/"
fi

PVR_BUILD_OPTIONS="PVR_USE_SRC_BSP=yes \
    PVR_MERGE_SRC=$devicedir/.pvr \
    PANTAVISOR_DEBUG=yes \
    $PVR_EXTRA_BUILD_OPTIONS \
"

# allow to skip steps by using CMD=<startstepname>
# hence we read_device_meta at each step again ....
case "$CMD" in
    "")
	echo "Doing a full run"
	;&
    clone_device)
	clone_device $pvrrepo $devicedir
	;&
    read_device_meta)
	read_device_meta $devicedir
	;&
    clone_device_manifest)
	read_device_meta $devicedir
	clone_device_manifest $repodir $devicedir $manifest_platform
	;&
    repo_sync)
	read_device_meta $devicedir
	repo_sync $repodir
	;&
    build_target)
	read_device_meta $devicedir
	build_target $manifest_target $repodir $devicedir
	;&
    assemble_pipeline)
	read_device_meta $devicedir
	assemble_pipeline $pipelinedir $repodir $manifest_platform
	;&
    make_release_json)
	read_device_meta $devicedir
	make_release_json $pipelinedir $s3jsonuri
	;&
    publish_pipeline)
	if [ -n "s3jsonuri" ]; then
	    read_device_meta $devicedir
	    publish_pipeline $pipelinedir $channel
	fi
	;;
    *)
	echo_err "ERROR: unknown CMD env"
	;;
esac

echo "Happiness can be found in ..."
echo "  - Index: $s3jsonuri"
echo "  - Published: ${s3urlprefix}${uploadfile}"

popd

if [ -z "$KEEPTMP" ] && [ -n "$imgdir" ]; then
    rm -rf $imgdir
fi
